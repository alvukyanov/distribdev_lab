package com.example.robots_idea.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Setter
@Getter
@ToString

@Entity
@Table(name="Robots")

public class Robot extends RepresentationModel<Robot>
{
    @Id
    @Column(name = "ID", nullable = false)
    private int id;
    @Column(name = "robotsName", nullable = false)
    private String robotsName;
    @Column(name = "axleload", nullable = false)
    private boolean axleload;
    @Column(name = "type", nullable = false)
    private String  type;
    @Column(name = "withtouchinput", nullable = false)
    private boolean withtouchinput;
    @Column(name = "factoryNumber", nullable = false)
    private int factoryNumber;

    public com.example.robots_idea.model.Robot withName(String RobotName) {
        this.setRobotsName(RobotName);
        return this;
    }

}
