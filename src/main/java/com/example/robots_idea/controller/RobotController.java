package com.example.robots_idea.controller;

import com.example.robots_idea.model.Robot;
import com.example.robots_idea.service.RobotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.robots_idea.config.ServiceConfig;

import java.util.Locale;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value="robots/{robotsName}/Robot")

public class RobotController {

    @Autowired
    private RobotService robotService;

    @Autowired
    private MessageSource messages;

    public ResponseEntity<Robot> getRobots(
            @PathVariable("robotsName") String robotsName,
            @PathVariable("axleload") boolean axleload) {
        Robot robots = robotService.getRobots(robotsName, axleload);
        robots.add(
                linkTo(methodOn(RobotController.class)
                        .getRobots(robots.getRobotsName(), axleload))
                        .withSelfRel(),
                linkTo(methodOn(RobotController.class)
                        .createRobots(robots))
                        .withRel("Create an entry in the robot register"),
                linkTo(methodOn(RobotController.class)
                        .changeRobots(robots))
                        .withRel("Update an entry in the robot register"),
                linkTo(methodOn(RobotController.class)
                        .deleteRobots(robots.getRobotsName(), axleload,null))
                        .withRel("Delete an entry in the robot register"));
        return ResponseEntity.ok(robots);

    }

        @GetMapping(value = "/robot/{id}")
    public ResponseEntity<Robot> getRobot(
            @PathVariable("id") int id,
            @RequestHeader(value = "Accept-Language", required = false) Locale locale
    ) {
        Robot robot = robotService.getRobot(id);
        robot.add(linkTo(methodOn(RobotController.class).getRobot(id, locale))
                        .withSelfRel(),
                linkTo(methodOn(RobotController.class)
                        .createRobot(robot.getFactoryNumber(), robot, locale))
                        .withRel(messages.getMessage("url.create.name", null, locale)),
                linkTo(methodOn(RobotController.class)
                        .changeRobot(id, robot, locale))
                        .withRel(messages.getMessage("url.update.name", null, locale)),
                linkTo(methodOn(RobotController.class)
                        .deleteRobot(id, locale))
                        .withRel(messages.getMessage("url.delete.name", null, locale)));
        return ResponseEntity.ok(robot);
    }

    @PostMapping(value = "/robot/{factoryNumber}")
    public ResponseEntity<String> createRobot(
            @PathVariable("factoryNumber") Integer factoryNumber,
            @RequestBody Robot request,
            @RequestHeader(value = "Accept-Language", required = false) Locale locale
    ) {
        return ResponseEntity.ok(robotService.createRobot(request, factoryNumber, locale));
    }
    @PutMapping(value = "/robot/{id}")
    public ResponseEntity<String> changeRobot(
            @PathVariable("id") Integer id,
            @RequestBody Robot request,
            @RequestHeader(value = "Accept-Language", required = false) Locale locale
    ) {
        return ResponseEntity.ok(robotService.changeRobot(request, id, locale));
    }
    @DeleteMapping(value = "/robot/{id}")
    public ResponseEntity<String> deleteRobot(
            @PathVariable("id") int id,
            @RequestHeader(value = "Accept-Language", required = false) Locale locale
    )
    {
        return ResponseEntity.ok(robotService.deleteRobot(id, locale));
    }
    @PostMapping
    public ResponseEntity<Robot> createRobots(
            @RequestBody Robot request) {
        return ResponseEntity.ok(robotService.createRobots(request));
    }

    @PutMapping
    public ResponseEntity<Robot> changeRobots(
            @RequestBody Robot request) {
        return ResponseEntity.ok(robotService.changeRobots(request));
    }


    @DeleteMapping(value="/{axleload}")
    public ResponseEntity<String> deleteRobots(
            @PathVariable("robotsName") String RobotName,
            @PathVariable("axleload") boolean axleload,
            @RequestHeader(value = "Accept-Language",required = false)
            Locale locale) {
        return ResponseEntity.ok(robotService.deleteRobots(RobotName, axleload, locale));
    }

}


