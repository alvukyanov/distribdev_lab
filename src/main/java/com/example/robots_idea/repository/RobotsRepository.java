package com.example.robots_idea.repository;

import com.example.robots_idea.model.Robot;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
import java.util.List;


@Repository
public interface RobotsRepository extends CrudRepository<Robot,String> {
    public List<Robot> findByRobotsName
            (String RobotName);
    public Robot findByRobotsNameAndAxleload
            (String RobotName,
             Boolean Axleload);
}

