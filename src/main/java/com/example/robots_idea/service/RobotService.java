package com.example.robots_idea.service;

import com.example.robots_idea.model.Robot;
import com.example.robots_idea.repository.RobotsRepository;
import com.example.robots_idea.config.ServiceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public class RobotService {

    @Autowired
    private MessageSource messages;
    @Autowired
    private RobotsRepository robotsRepository;
    @Autowired
    ServiceConfig config;

    public Robot getRobots(String RobotName, boolean AxleloadType) {
        Robot Robots = robotsRepository.findByRobotsNameAndAxleload(RobotName, AxleloadType);
        if (null == Robots) {
            throw new IllegalArgumentException(String.format(
                    messages.getMessage(
                            "Robots.search.error.message", null, null),
                    AxleloadType, RobotName));
        }
        return Robots.withName(config.getProperty());
    }

    public Robot createRobots(Robot robot){
        robotsRepository.save(robot);
        return robot.withName(config.getProperty());
    }

    public Robot changeRobots(Robot robot){
        robotsRepository.save(robot);
        return robot.withName(config.getProperty());
    }

    public String deleteRobots(String RobotName, boolean axleload, Locale locale) {
        String responseMessage = null;
        Robot robot = new Robot();
        robot.setRobotsName(RobotName);
        robot.setAxleload(axleload);
        robotsRepository.delete(robot);
        responseMessage = String.format(messages.getMessage("robots.delete.message", null, locale), axleload, RobotName);
        return responseMessage;
    }



    public Robot getRobot( int id){
            Robot robot = new Robot();
            robot.setId(id);
            robot.setRobotsName("Kevin");
            robot.setAxleload(true);
            robot.setType("Interactive robots");
            robot.setWithtouchinput(true);
            robot.setFactoryNumber(12);
            return robot;
        }

    public String createRobot(Robot robot, Integer factoryNumber, Locale locale){
            String responseMessage = null;
            if (robot != null) {
                robot.setFactoryNumber(factoryNumber);
                responseMessage = String.format(messages.getMessage("robot.create.message", null, locale),
                        robot.getId(), robot);
            }
            return responseMessage;
        }

    public String changeRobot(Robot robot, Integer id, Locale locale){
            String responseMessage = null;
            if (robot != null) {
                responseMessage = String.format(messages.getMessage("robot.update.message", null, locale),
                        id, robot);
            }
            return responseMessage;
        }

    public String deleteRobot(Integer id, Locale locale){
            return String.format(messages.getMessage("robot.delete.message", null, locale), id);
        }
}
